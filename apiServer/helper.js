// data must have prop name, job, startDate and endDate,
// time between start date and end date no large than two year;
const checkStaffDataValid = (data) => {
  if (data.name && data.job && data.startDate && data.endDate
    && typeof data.startDate === 'number' && typeof data.endDate === 'number'
    && (data.endDate - data.startDate) <= 3600 * 24 * 365 * 2 * 1000
  ) {
    return true;
  }
  return false;
};

module.exports = {
  checkStaffDataValid,
};
