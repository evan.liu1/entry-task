/* eslint-disable */
const CryptoJS = require("crypto-js");
const { checkStaffDataValid } = require('../../helper');
const { errorCode, errorCodeMap } = require('../../errorCode');

const log = (lg)=>{
  // console.log(lg)
}

module.exports = (function () {
  let id = 100001;
  const staff = [];
  const admin = [
    {
      id: 1,
      type: 1,
      name: 'admin',
      password: '21232f297a57a5a743894a0e4a801fc3', // 'admin'
    },
    {
      id: 2,
      type: 2,
      name: 'test',
      password: '098f6bcd4621d373cade4e832627b4f6', // 'test'
    },
  ];
  //token => admin id, expire time
  const tokenMap = {

  }
  //refresh token => token, expire time
  const refreshTokenMap = {

  }

  let nameMap = {}; // use for fast detect user exist
  const updateNameMap = () => {
    nameMap = {};
    staff.forEach((item) => {
      nameMap[item.name] = true;
    });
  };

  // add staff array
  const addStaffArray = (data) => {
    const tempAddArray = [];
    const tempNameMap = {};
    data.forEach((item) => {
      if (!checkStaffDataValid(item)) {
        error = errorCode.parameterError;
      } else if (nameMap[item.name]) {
        error = errorCode.staffExist;
      } else if (tempNameMap[item.name]) {
        error = errorCode.staffNameShouldNotBeTheSame;
      } else {
        const add = Object.assign(item, {
          duration: item.endDate - item.startDate,
          id: id++,
        });
        tempNameMap[item.name] = true;
        tempAddArray.push(add);
      }
    });
    // make sure commit complete
    if (tempAddArray.length !== data.length) {
      return {
        success: false,
        errorCode: error,
        errorDetail: errorCodeMap.get(error),
      };
    }
    staff.unshift(...tempAddArray);
    updateNameMap();
    return {
      success: true,
    };
  };

  // add single staff data
  const addSingleStaff = (data) => {
    if (!checkStaffDataValid(data)) {
      return {
        success: false,
        errorCode: errorCode.parameterError,
        errorDetail: errorCodeMap.get(errorCode.parameterError),
      };
    } if (nameMap[data.name]) {
      return {
        success: false,
        errorCode: errorCode.staffExist,
        errorDetail: errorCodeMap.get(errorCode.staffExist),
      };
    }
    const add = Object.assign(data, {
      duration: data.endDate - data.startDate,
      id: id++,
    });
    staff.unshift(add);
    updateNameMap();
    return {
      success: true,
    };
  };

  const addStaff = (data) => {
    if (data && data instanceof Array) {
      // add with array
      return addStaffArray(data);
    }
    // add with single staff
    return addSingleStaff(data);
  };

  // return staff with option filter: name, job, startDate and endDate;
  const getStaff = (page, size, filter) => {
    const start = (page - 1) * size;
    let list = [];
    let pendingData = staff;
    // if filter is not empty
    if (filter && (filter.name || filter.job || filter.startDate || filter.endDate)) {
      pendingData = [];
      for (let i = 0; i < staff.length; i++) {
        const item = staff[i];
        log(item);
        if (filter.name) {
          // fuzzy search name
          log(item.name);
          log("filter.name:" + filter.name);
          if (item.name.indexOf(filter.name) < 0) {
            log('skip name filter');
            continue;
          }
        }
        if (filter.job) {
          log("filter.job:" + filter.job);
          // job value from job map, keep the same with front end side
          if (`${item.job}` !== `${filter.job}`) {
            continue;
          }
        }
        if (filter.startDate && filter.endDate) {
          log("filter.startDate:" + filter.startDate);
          log("filter.endDate:" + filter.endDate);
          // if filter contain startDate and endDate, just make sure they have intersection
          if (item.endDate < filter.startDate) {
            continue;
          }
          if (item.startDate > filter.endDate) {
            continue;
          }
        } else if (filter.startDate) {
          log("filter.startDate:" + filter.startDate);
          // if filter only contain start date remove all end date early than filter's start date
          if (item.endDate < filter.startDate) {
            continue;
          }
        } else if (filter.endDate) {
          log("filter.endDate:" + filter.endDate);
          // if filter only contain end date remove all end date early than filter's start date
          if (item.startDate > filter.endDate) {
            continue;
          }
        }
        pendingData.push(item);
      }
    }
    log(pendingData);
    const { length } = pendingData;
    // make sure last item can be include
    const total = Math.ceil(length / size);
    if (start < length) {
      const end = start + size < length ? start + size : length;
      list = pendingData.slice(start, end);
    }
    return {
      success: true,
      data: list,
      total,
    };
  };

  const deleteStaff = (id) => {
    let target = -1;
    for(let i = 0; i < staff.length; i++){
      if(staff[i].id+'' === id+''){
        target = i;
        break;
      }
    }
    if(target >= 0){
      staff.splice(target,1);
      updateNameMap();
      return {
        success: true,
      };
    }else {
      return {
        success: false,
        errorCode: errorCode.staffNotExist,
        errorDetail: errorCodeMap.get(errorCode.staffNotExist),
      };
    }
  }

  const updateStaff = (id, name) => {
    let target = -1;
    for(let i = 0; i < staff.length; i++){
      if(staff[i].id+'' === id+''){
        target = i;
        break;
      }
    }
    if(target >= 0){
      if(!name){
        return {
          success: false,
          errorCode: errorCode.staffNameInvalid,
          errorDetail: errorCodeMap.get(errorCode.staffNameInvalid),
        };
      }
      if (nameMap[name]) {
        return {
          success: false,
          errorCode: errorCode.staffExist,
          errorDetail: errorCodeMap.get(errorCode.staffExist),
        };
      }
      staff[target].name = name;
      updateNameMap();
      return {
        success: true,
      };
    }else {
      return {
        success: false,
        errorCode: errorCode.staffNotExist,
        errorDetail: errorCodeMap.get(errorCode.staffNotExist),
      };
    }
  }

  return {
    addStaff,
    getStaff,
    updateStaff,
    deleteStaff,
  };
}());
