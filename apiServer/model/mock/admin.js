/* eslint-disable */
const CryptoJS = require("crypto-js");
const TokenGenerator = require('uuid-token-generator');
// tokgen.generate();
const tokgen = new TokenGenerator();
const { checkStaffDataValid } = require('../../helper');
const { errorCode, errorCodeMap } = require('../../errorCode');
const { userType, permissionMap } = require('../../constants');


const log = (lg)=>{
	// console.log(lg)
}

const getCurrentTime = ()=>{
	const date = new Date();
	return date.getTime();
}

module.exports = (function () {
	const admin = [
		{
			id: 1,
			type: userType.ADMIN,
			name: 'admin',
			email: 'admin@shoppe.com',
			password: '21232f297a57a5a743894a0e4a801fc3', // 'admin'
		},
		{
			id: 2,
			type: userType.TEST,
			name: 'test',
			email: 'test@shoppe.com',
			password: '098f6bcd4621d373cade4e832627b4f6', // 'test'
		},
	];
	// token => admin id, refresh token, expire time(one day)
	const tokenMap = {

	}
	// refresh token => admin id, token, expire time(3 days)
	const refreshTokenMap = {

	}
  // after user login in other device, delete exist token for this user
	const deleteTokenInfoByAdminId = (id)=>{

	}

	// login
	const login = (name, password) => {
		if(!name || !password){
			return {
				success: false,
				errorCode: errorCode.parameterError,
				errorDetail: errorCodeMap.get(errorCode.parameterError),
			};
		}
		const md5Password = CryptoJS.MD5(password).toString();
		let targetIndex = -1;
		for(let i = 0; i < admin.length; i++){
			if(admin[i].name === name && admin[i].password === md5Password){
				targetIndex = i;
				break;
			}
		}
		if(targetIndex < 0){
			//user name or password error
			return {
				success: false,
				errorCode: errorCode.userNameOrPasswordError,
				errorDetail: errorCodeMap.get(errorCode.userNameOrPasswordError),
			};
		}

		const info = admin[targetIndex];
		const token = tokgen.generate();
		const refreshToken = tokgen.generate();
		const tokenExpire = getCurrentTime() + 3600 * 24 * 1000;
		tokenMap[token] = {
			id: info.id,
			refreshToken,
			expired: tokenExpire, // expired on one day
		}
		refreshTokenMap[refreshToken] = {
			id: info.id,
			token,
			expired: getCurrentTime() + 3600 * 24 * 1000 * 3,
		}
		return {
			success: true,
			info: {
				id: info.id,
				type: info.type,
				name: info.name,
				expired: tokenExpire,
				email: info.email,
				token,
				refreshToken: tokenMap[token].refreshToken,
			}
		};
	};

	const logout = (token) => {
		const tokenObject = tokenMap[token];
		if(tokenObject){
			const refreshToken = tokenObject['refreshToken'];
			if(refreshToken && refreshTokenMap[refreshToken]){
				delete refreshTokenMap[refreshToken];
			}
			delete tokenMap[token];
		}
		return {
			success: true,
		}
	};

	// get user info
	const getInfo = (token) => {
		const tokenObject = tokenMap[token];
		log(tokenMap);
		if(tokenObject){
			if(tokenObject['expired'] < getCurrentTime()){
				delete tokenMap[token];
				return {
					success: false,
					errorCode: errorCode.statusExpired,
					errorDetail: errorCodeMap.get(errorCode.statusExpired),
				}
			}
			const id = tokenObject['id'];
			let targetIndex = -1;
			for(let i = 0; i < admin.length; i++){
				if(admin[i].id === id){
					targetIndex = i;
					break;
				}
			}
			if(targetIndex < 0){
				//user deleted
				return {
					success: false,
					errorCode: errorCode.statusExpired,
					errorDetail: errorCodeMap.get(errorCode.statusExpired),
				};
			}
			const info = admin[targetIndex];
			return {
				success: true,
				info: {
					id: info.id,
					type: info.type,
					name: info.name,
					email: info.email,
					token,
					expired: tokenObject.expired,
					refreshToken: tokenMap[token].refreshToken,
				}
			};
		}else {
			return {
				success: false,
				errorCode: errorCode.statusExpired,
				errorDetail: errorCodeMap.get(errorCode.statusExpired),
			};
		}
	};

	// refresh user token
	const refreshToken = (refreshToken) => {
		const refreshTokenObject = refreshTokenMap[refreshToken]
		if(refreshTokenObject){
			const expired = refreshTokenObject['expired'];
			const currentTime = getCurrentTime();
			if(currentTime > expired){
				// refresh token is expired
				delete refreshTokenMap[refreshToken]
				return {
					success: false,
					errorCode: errorCode.refreshTokenExpired,
					errorDetail: errorCodeMap.get(errorCode.refreshTokenExpired),
				};
			}
			const adminId = refreshTokenObject['id'];
			const oldToken = refreshTokenObject['token'];
			// delete old token
			delete tokenMap[oldToken];

			let targetIndex = -1;
			for(let i = 0; i < admin.length; i++){
				if(admin[i].id === adminId){
					targetIndex = i;
					break;
				}
			}
			if(targetIndex < 0){
				//user deleted
				return {
					success: false,
					errorCode: errorCode.statusExpired,
					errorDetail: errorCodeMap.get(errorCode.statusExpired),
				};
			}
			// update token map
			const info = admin[targetIndex];
			const newToken = tokgen.generate();
			const newTokenExpired = getCurrentTime() + 3600 * 24 * 1000;
			tokenMap[newToken] = {
				id: info.id,
				refreshToken,
				expired: newTokenExpired, // expired on one day
			}

			// extend expired time for current refresh token
			refreshTokenObject.token = newToken;
			refreshTokenObject.expired = getCurrentTime() + 3600 * 24 * 1000 * 3;

			return {
				success: true,
				token: newToken,
				expired: newTokenExpired,
			};
		}else {
			return {
				success: false,
				errorCode: errorCode.refreshTokenExpired,
				errorDetail: errorCodeMap.get(errorCode.refreshTokenExpired),
			};
		}
	};

	const checkPermission = (token, action)=>{
		const availablePermission = permissionMap.get(action);
		if(!availablePermission){
			//not need any permission
			return {
				success: true,
			}
		}
		const tokenObject = tokenMap[token];
		if(tokenObject) {
			if(tokenObject['expired'] < getCurrentTime()){
				delete tokenMap[token];
				return {
					success: false,
					errorCode: errorCode.statusExpired,
					errorDetail: errorCodeMap.get(errorCode.statusExpired),
				}
			}
			const id = tokenObject['id'];
			let targetIndex = -1;
			for (let i = 0; i < admin.length; i++) {
				if (admin[i].id === id) {
					targetIndex = i;
					break;
				}
			}
			if (targetIndex < 0) {
				//user deleted
				return {
					success: false,
					errorCode: errorCode.statusExpired,
					errorDetail: errorCodeMap.get(errorCode.statusExpired),
				};
			}
			const info = admin[targetIndex];
			if (availablePermission.indexOf(info.type) < 0) {
				return {
					success: false,
					errorCode: errorCode.userHasNoPermission,
					errorDetail: errorCodeMap.get(errorCode.userHasNoPermission),
				};
			} else {
				return {
					success: true,
				};
			}
		}
		return {
			success: false,
			errorCode: errorCode.statusExpired,
			errorDetail: errorCodeMap.get(errorCode.statusExpired),
		};
	}

	return {
		getInfo,
		login,
		logout,
		refreshToken,
		checkPermission,
	};
}());
