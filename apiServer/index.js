/* eslint-disable */
const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const route = require('koa-route');
const staffModel = require('./model/mock/staff');
const adminModel = require('./model/mock/admin');
const { userAction, permissionMap } = require('./constants');
const port = 3201;
const app = new Koa();

app.use(cors());
app.use(bodyParser());

//create staff, support single and mass
const addStaff = async (ctx) => {
  const token = ctx.request.headers['authorization'];
  const checkPermissionResult = adminModel.checkPermission(token, userAction.ADD_STAFF);
  if(!checkPermissionResult.success){
    ctx.response.body = {
      ...checkPermissionResult
    };
    return ;
  }
  const postData = ctx.request.body;
  const addResult = staffModel.addStaff(postData);
  ctx.response.type = 'json';
  ctx.response.body = {
    ...addResult
  };
};

//get staff with filter and pagination
const getStaff = (ctx) => {
  ctx.response.type = 'json';
  let page = ctx.request.query.currentPage || 1;
  page = parseInt(page);
  let size = ctx.request.query.size || 4;
  size = parseInt(size);
  let filter = ctx.request.query.filter || null;
  try{
    filter = JSON.parse(filter);
  }catch (e) {
    console.log(e)
  }
  const staffResult = staffModel.getStaff(page, size, filter);
  if(staffResult.success){
    ctx.response.body = {
      success: true,
      list: staffResult.data,
      totalPage: staffResult.total,
      currentPage: page,
    }
  }else {
    // error happen
    ctx.response.body = {
      ...staffResult
    };
  }
};

//delete staff by id
const deleteStaff = (ctx, id) => {
  const token = ctx.request.headers['authorization'];
  const checkPermissionResult = adminModel.checkPermission(token, userAction.DELETE_STAFF);
  if(!checkPermissionResult.success){
    ctx.response.body = {
      ...checkPermissionResult
    };
    return ;
  }
  ctx.response.type = 'json';
  const deleteResult = staffModel.deleteStaff(id);
  if(deleteResult.success){
    ctx.response.body = {
      success: true,
    }
  }else {
    // error happen
    ctx.response.body = {
      ...deleteResult
    };
  }
};

//update staff name by id
const updateStaff = async (ctx, id) => {
  const token = ctx.request.headers['authorization'];
  const checkPermissionResult = adminModel.checkPermission(token, userAction.EDIT_STAFF);
  if(!checkPermissionResult.success){
    ctx.response.body = {
      ...checkPermissionResult
    };
    return ;
  }
  ctx.response.type = 'json';
  const postData = ctx.request.body;
  const name = postData.name;
  const updateResult = staffModel.updateStaff(id, name);
  if(updateResult.success){
    ctx.response.body = {
      success: true,
    }
  }else {
    // error happen
    ctx.response.body = {
      ...updateResult
    };
  }
};

app.use(route.put('/staff/add', addStaff));
app.use(route.get('/staff/get', getStaff));
app.use(route.post('/staff/update/:id', updateStaff));
app.use(route.delete('/staff/delete/:id', deleteStaff));

// user login relative

const login = async (ctx) => {
  ctx.response.type = 'json';
  const postData = ctx.request.body;
  const name = postData.name;
  const password = postData.password;
  const result = adminModel.login(name, password);
  ctx.response.body = {
    ...result
  }
};

const getInfo = async (ctx) => {
  ctx.response.type = 'json';
  const token = ctx.request.headers['authorization']
  const result = adminModel.getInfo(token);
  ctx.response.body = {
    ...result
  }
};

const refreshToken = async (ctx) => {
  ctx.response.type = 'json';
  const postData = ctx.request.body;
  const refreshToken = postData.refreshToken;
  //console.log("token:" + token);
  const result = adminModel.refreshToken(refreshToken);
  ctx.response.body = {
    ...result
  }
};

const logout = async (ctx) => {
  ctx.response.type = 'json';
  const token = ctx.request.headers['authorization']
  const result = adminModel.logout(token);
  ctx.response.body = {
    ...result
  }
};

app.use(route.post('/admin/login', login));
app.use(route.post('/admin/logout', logout));
app.use(route.post('/admin/token/refresh', refreshToken));
app.use(route.get('/admin/info', getInfo));


console.log(`Server start on: ${port}`);
app.listen(port);
