const userType = {
  ADMIN: 1,
  TEST: 2,
};

const userAction = {
  ADD_STAFF: 'userAction/ADD_STAFF',
  EDIT_STAFF: 'userAction/ADD_STAFF',
  DELETE_STAFF: 'userAction/ADD_STAFF',
};

const permissionMap = new Map();

permissionMap.set(userAction.ADD_STAFF, [userType.ADMIN]);
permissionMap.set(userAction.EDIT_STAFF, [userType.ADMIN]);
permissionMap.set(userAction.DELETE_STAFF, [userType.ADMIN]);

module.exports = {
  userType,
  permissionMap,
  userAction,
};
