const errorCodeMap = new Map();
const errorCode = {
  parameterError: 10001,
  staffExist: 10002,
  staffNameShouldNotBeTheSame: 10003,
  staffNotExist: 10004,
  staffNameInvalid: 10005,
  statusExpired: 10006,
  userNameOrPasswordError: 10007,
  refreshTokenExpired: 10008,
  userNotExist: 10009,
  userHasNoPermission: 10010,
};
errorCodeMap.set(errorCode.parameterError, 'Input parameter is error');
errorCodeMap.set(errorCode.staffExist, 'Staff is exist');
errorCodeMap.set(errorCode.staffNameShouldNotBeTheSame, 'Staff name should not be the same');
errorCodeMap.set(errorCode.staffNotExist, 'Staff is not exist');
errorCodeMap.set(errorCode.staffNameInvalid, 'Staff name is invalid');
errorCodeMap.set(errorCode.userNotExist, 'User is not exit');
errorCodeMap.set(errorCode.statusExpired, 'Status is expired');
errorCodeMap.set(errorCode.refreshTokenExpired, 'Refresh token is expired');
errorCodeMap.set(errorCode.userNameOrPasswordError, 'User name or password error');
errorCodeMap.set(errorCode.userHasNoPermission, 'User do not have permission for this action');

module.exports = {
  errorCodeMap,
  errorCode,
};
