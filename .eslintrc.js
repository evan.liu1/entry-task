module.exports = {
  "parser": "babel-eslint",
  "extends": "airbnb",
  "env": {
    "browser": true,
    "node": true,
    "mocha": true,
    "es6": true
  },
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
      "experimentalObjectRestSpread": true
    }
  },
  "rules": {
    "no-console": 0,
    "import/prefer-default-export": 0,
    "implicit-arrow-linebreak": 0,
    "prefer-object-spread": 0,
    'jsx-a11y/no-static-element-interactions': 0,
    "react/prop-types": 0,
    "react/jsx-indent": 2,
    "react/jsx-curly-newline": 0,
    "react/state-in-constructor": 0,
    "react/static-property-placement": 0,
    "react/jsx-props-no-spreading": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "react/button-has-type": 0,
    "import/no-unresolved": 0,
    "react/jsx-filename-extension": 0,
  }
}
