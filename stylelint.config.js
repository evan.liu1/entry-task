/* eslint-disable no-bitwise */
module.exports = {
  processors: [],
  plugins: [],
  extends: 'stylelint-config-standard', // 这是官方推荐的方式
  rules: {
    'block-no-empty': true,
    'no-duplicate-selectors': null,
    'selector-pseudo-class-no-unknown': [true, {
      ignorePseudoClasses: ['global'],
    }],
  },
};
