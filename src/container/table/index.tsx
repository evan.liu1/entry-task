/* eslint-disable no-unused-vars */
import * as React from 'react';
import { connect } from 'react-redux';
import {
  DatePicker, Input, Button, Select, Pagination, Modal,
} from '@shopee/antd';
// component import
// @ts-ignore
import StaffTable from '../../component/staffTable';
import CreateModal from '../../component/modal/create';

import staffActions from '../../data/staff/actions';
import style from './style.scss';
// available job title map
import { jobMap, requestStatus } from '../../utils/constants';
import { IAdminInfo, IFilter } from '../../utils/interface';
import { getPermission, isShallowSame } from '../../utils/helperFun';
import { usePrevious } from '../../utils/hook';

const { Option } = Select;
const { RangePicker } = DatePicker;
const { useState, useEffect } = React;

interface IProp {
  staff: {
    name: string;
    job: string;
    duration: string
  }[];
  addStaff: Function;
  getStaffList: Function;
  totalPage: number,
  info: IAdminInfo,
  getStaffRequestStatus: string,
  deleteStaffRequestStatus: string,
  addStaffRequestStatus: string,
  updateStaffRequestStatus: string,
}
// todo better use useCallback for event binding
const Table = (props: IProp) => {
  // eslint-disable-next-line no-unused-vars
  const {
    staff, addStaff, getStaffList, totalPage,
    getStaffRequestStatus, deleteStaffRequestStatus,
    updateStaffRequestStatus, addStaffRequestStatus,
    info,
  } = props;
  const [name, setName] = useState('');
  const [jobValue, setJobValue] = useState(undefined);
  // todo find moment js ts define
  const [startTime, setStartTime] = useState<any>(undefined);
  const [endTime, setEndTime] = useState<any>(undefined);
  // eslint-disable-next-line no-unused-vars
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState<any>(4);
  const [latestFilter, setLatestFilter] = useState<any>(undefined);
  const [showModal, setShowModal] = useState(false);
  const preRequestStatus:any = usePrevious({
    deleteStaffRequestStatus,
    addStaffRequestStatus,
  });
  useEffect(() => {
    if (info && info.id) {
      // update when user login or get info success
      getStaffList(currentPage, pageSize);
    }
  }, [info]);
  const getFilter = () => {
    const filter: IFilter = {};
    if (name) {
      filter.name = name;
    }
    if (jobValue) {
      filter.job = jobValue;
    }
    if (startTime) {
      filter.startDate = startTime.valueOf();
    }
    if (endTime) {
      filter.endDate = endTime.valueOf();
    }
    if (Object.keys(filter).length === 0) {
      return null;
    }
    return filter;
  };

  // do refresh when success delete, update or add
  useEffect(() => {
    // after delete success
    if (preRequestStatus && preRequestStatus.deleteStaffRequestStatus === requestStatus.REQUESTING
      && deleteStaffRequestStatus === requestStatus.SUCCESS
    ) {
      const filter = getFilter();
      getStaffList(currentPage, pageSize, filter);
    }

    // after add success
    if (preRequestStatus && preRequestStatus.addStaffRequestStatus === requestStatus.REQUESTING
        && addStaffRequestStatus === requestStatus.SUCCESS
    ) {
      const filter = getFilter();
      getStaffList(currentPage, pageSize, filter);
    }
  }, [deleteStaffRequestStatus, addStaffRequestStatus]);

  const jobKeys = Array.from(jobMap.keys());
  const targetKey = `_${currentPage}`;
  const list = staff[targetKey];
  const loading = getStaffRequestStatus === requestStatus.REQUESTING;
  const permission = getPermission(info);
  return (
    <div className={style.wrap}>
      <h2 className={style.pageTitle}>Table</h2>
      <div className={style.inputWrap}>
        <div className={style.inputRow}>
          <div className={style.inputItem}>
            <span className={style.inputItemLabel}>Name</span>
            <Input
              value={name}
              maxLength={30}
              onChange={(e) => {
                setName(e.target.value);
              }}
              className={style.inputItemContent}
              placeholder="String Only"
            />
          </div>
          <div className={style.inputItem}>
            <span className={style.inputItemLabel}>Job Description</span>
            <Select
              showSearch
              placeholder="Select"
              className={style.inputItemContent}
              value={jobValue}
              onChange={(value) => {
                setJobValue(value);
              }}
            >
              {
                  jobKeys.map((key) => (
                    <Option value={key} key={key}>{jobMap.get(key)}</Option>
                  ))
                }
            </Select>
          </div>
        </div>
        <div className={style.inputRow}>
          <div className={style.inputItem}>
            <span className={style.inputItemLabel}>Entry Date</span>
            <RangePicker
              placeholder={['Start Date', 'End Date']}
              className={style.inputItemContent}
              separator={'->'}
              allowClear={false}
              value={[startTime, endTime]}
              onChange={(dates) => {
                if (dates && dates.length === 2) {
                  const start = dates[0];
                  const end = dates[1];
                  if (start && end) {
                    setStartTime(start);
                    setEndTime(end);
                  }
                }
              }}
            />
          </div>
          <div className={style.inputButtonItem}>
            <Button
              onClick={() => {
                const filter = getFilter();
                let page = currentPage;
                // when filter change, get first page data
                if (!isShallowSame(filter, latestFilter)) {
                  page = 1;
                  setCurrentPage(1);
                }
                getStaffList(page, pageSize, filter);
              }}
              type="primary"
              className={style.inputItemButton}
            >
                Submit
            </Button>
            <Button
              onClick={() => {
                setEndTime(undefined);
                setStartTime(undefined);
                setName('');
                setJobValue(undefined);
                // maybe need to reload when reset
                // getStaffList(currentPage, pageSize);
              }}
              className={style.inputItemButton}
            >
                Reset
            </Button>
          </div>
        </div>
      </div>
      {permission.addStaff
      && (
      <div>
        <Button
          onClick={() => {
            setShowModal(true);
          }}
          type="primary"
          className={style.inputItemButton}
        >
          Create
        </Button>
      </div>
      )
      }
      <div className={style.tableWrap}>
        <StaffTable data={list} isLoading={loading} editable={permission.editStaff} />
      </div>
      <div className={style.paginationWrap}>
        <Pagination
          onShowSizeChange={(page, size) => {
            if (size !== pageSize) {
              const filter = getFilter();
              getStaffList(page, size, filter);
              setPageSize(size);
            }
          }}
          onChange={(page, size) => {
            if (currentPage !== page) {
              const filter = getFilter();
              getStaffList(page, size, filter);
              setCurrentPage(page);
            }
          }}
          defaultPageSize={pageSize}
          current={currentPage}
          pageSizeOptions={['4', '8', '12']}
          total={totalPage * pageSize}
          showSizeChanger
          showQuickJumper
        />
      </div>
      <CreateModal
        show={showModal}
        onCancel={() => {
          setShowModal(false);
        }}
        onOk={() => {
          setShowModal(false);
        }}
      />
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  staff: state.staff.staff,
  info: state.admin.info,
  totalPage: state.staff.totalPage,
  getStaffRequestStatus: state.staff.getStaffRequestStatus,
  addStaffRequestStatus: state.staff.addStaffRequestStatus,
  deleteStaffRequestStatus: state.staff.deleteStaffRequestStatus,
  updateStaffRequestStatus: state.staff.deleteStaffRequestStatus,
});

const mapDispatchToProps = (dispatch: any) => ({
  addStaff: (data: { name: string; job: string; startDate: number, endDate: number }) => {
    dispatch(staffActions.addStaff(data));
  },
  getStaffList: (currentPage: number, size: number, filter: null | IFilter) =>
    dispatch(staffActions.getStaffList(currentPage, size, filter)),
});

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Table);
