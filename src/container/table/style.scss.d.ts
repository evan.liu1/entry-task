declare namespace StyleScssNamespace {
  export interface IStyleScss {
    inputButtonItem: string;
    inputItem: string;
    inputItemButton: string;
    inputItemContent: string;
    inputItemLabel: string;
    inputRow: string;
    inputWrap: string;
    pageTitle: string;
    paginationWrap: string;
    tableWrap: string;
    timeError: string;
    wrap: string;
  }
}

declare const StyleScssModule: StyleScssNamespace.IStyleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StyleScssNamespace.IStyleScss;
};

export = StyleScssModule;
