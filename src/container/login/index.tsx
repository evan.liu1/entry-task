import * as React from 'react';
import { connect } from 'react-redux';
import { Button, Input } from '@shopee/antd';
import style from './style.scss';
import adminActions from '../../data/admin/actions';
import { IServerLoginRequest } from '../../utils/interface';
import { requestStatus } from '../../utils/constants';

const { useState } = React;

interface IProps {
  login:Function,
  loginRequestStatus: string,
}

const Login = (props:IProps) => {
  const { login, loginRequestStatus } = props;
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const loading = loginRequestStatus === requestStatus.REQUESTING;
  return (
    <div className={style.wrap}>
      <input
        type="text"
        name="_prevent_auto_complete_name"
        autoComplete="off"
        readOnly
        style={{ display: 'none' }}
      />
      <input
        type="password"
        name="_prevent_auto_complete_pass"
        autoComplete="new-password"
        readOnly
        style={{ display: 'none' }}
      />
      <div className={style.loginWrap}>
        <h3 className={style.loginTitle}>Login</h3>
        <div className={style.loginContent}>
          <div className={style.loginItem}>
            <Input
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
              maxLength={20}
              size="large"
              onKeyPress={(e) => {
                if (e.key === 'Enter' && name && password) {
                  login({ name, password });
                }
              }}
              className={style.inputItemContent}
              placeholder="User name"
            />
          </div>
          <div className={style.loginItem}>
            <Input
              value={password}
              type="password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              onKeyPress={(e) => {
                if (e.key === 'Enter' && name && password) {
                  login({ name, password });
                }
              }}
              size="large"
              className={style.inputItemContent}
              placeholder="User password"
            />
          </div>
          <div className={style.loginItem}>
            <Button
              block
              loading={loading}
              size="large"
              onClick={() => {
                login({ name, password });
              }}
              type="primary"
              className={style.inputItemContent}
            >
                  Submit
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  loginRequestStatus: state.admin.loginRequestStatus,
});

const mapDispatchToProps = (dispatch) => ({
  login: (data: IServerLoginRequest) => {
    dispatch(adminActions.login(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
