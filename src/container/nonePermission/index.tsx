import * as React from 'react';
import nonePermissionImage from './none-permisson.png';
import style from './style.scss';

const NonePermission = () => (
  <div className={style.wrap}>
    <img src={nonePermissionImage} alt="none permission" />
  </div>
);

export default NonePermission;
