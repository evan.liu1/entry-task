import * as React from 'react';

const { useRef, useEffect } = React;
const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

export {
  usePrevious,
};
