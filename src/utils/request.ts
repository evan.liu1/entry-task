import axios from 'axios';
import axiosRetry from 'axios-retry';
import { message } from '@shopee/antd';
import {baseUrl, errorCode} from './constants';
import {IServerCommonFail} from "./interface";
import {getRefreshToken, getToken} from "./helperFun";

axiosRetry(axios, {
  retries: 2,
  retryCondition: () => true,
  retryDelay: () => 2000,
});

const delay = (ms: any) => new Promise((resolve) => setTimeout(resolve, ms));

interface IConfig {
  showLoading?: boolean,
  method?: string,
  params?: Object,
  data?: Object,
  customErrorHandle?: boolean,
  headers?: Object,
}

const request = async (url, config?:IConfig, errorCallback?:Function) => {
  const token = getToken();
  const defaultConfig = {
    url,
    baseURL: baseUrl,
    timeout: 18000,
    validateStatus(status) {
      return status <= 500;
    },
    headers: { authorization: `${token}` }
  };
  const customErrorHandle = config && config.customErrorHandle;
  const requestConfig = config ? Object.assign(defaultConfig, config) : defaultConfig;

  // @ts-ignore
  return axios.request(requestConfig).then((result) => {
    if (!result || (result.status !== 200 && result.status !== 201) || !result.data.success) {
      const failResult = <IServerCommonFail>result.data;

      // if status expire try to refresh token if refresh token exist
      if(failResult.errorCode === errorCode.statusExpired){
        const refreshToken = getRefreshToken();
        if(refreshToken){

        }
      }

      const label = failResult.errorDetail;
      if (!customErrorHandle) {
        message.error(label)
      }
    }
    return result;
  }).catch((e) => {
    console.log(e);
    if (errorCallback) {
      errorCallback(e);
    }

    if(!customErrorHandle){
      const displayMessage = 'Server is no response';
      message.error(displayMessage);
    }
    return {};
  });
};

export default request;
