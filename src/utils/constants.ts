export const isDev = process && (process.env.NODE_ENV === 'development')

const requestStatus = {
  SUCCESS: 'success',
  FAIL: 'fail',
  IDLE: 'idle',
  REQUESTING: 'requesting',
};

const jobMap = new Map();
jobMap.set(1, 'Front-End Dev');
jobMap.set(2, 'Core Server');
jobMap.set(3, 'Product Manager');
jobMap.set(4, 'Project Manager');
jobMap.set(5, 'QA');
jobMap.set(5, 'Dev Ops');

// update to pro server host if need
const baseUrl = isDev ? 'http://localhost:3201' : 'http://localhost:3201';

const routerPath = {
  HOME: '/',
  TABLE: '/table',
  CHART: '/chart',
  LOGIN: '/login',
  NO_PERMISSION: '/no-permission',
};

const userType = {
  ADMIN: 1,
  NORMAL: 2,
};

const errorCode = {
  parameterError: 10001,
  staffExist: 10002,
  staffNameShouldNotBeTheSame: 10003,
  staffNotExist: 10004,
  staffNameInvalid: 10005,
  statusExpired: 10006,
  userNameOrPasswordError: 10007,
  refreshTokenExpired: 10008,
  userNotExist: 10009,
};

export {
  requestStatus,
  jobMap,
  baseUrl,
  routerPath,
  userType,
  errorCode,
};
