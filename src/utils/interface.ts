interface IFilter {
    name?: string;
    job?: number;
    startDate?: number;
    endDate?: number;
}

interface IPermission {
    addStaff: boolean;
    editStaff: boolean;
    showMenu: boolean;
}

interface ITokenInfo {
    token: string,
    expired: number,
    refreshToken: string,
}

interface IStaff {
    id: number;
    name: string;
    job: string;
    startDate: number;
    endDate: number;
    duration: number;
}

// request format to server
interface IServerAddStaffRequest {
    name: string;
    job: string;
    startDate: number;
    endDate: number
}

interface IServerUpdateStaffRequest {
    name: string;
}

interface IServerGetStaffRequest {
    currentPage: number;
    size: number;
    filter?: IFilter
}

interface IServerLoginRequest {
    name: string;
    password: string;
}

interface IServerRefreshTokenRequest {
    refreshToken: string;
}

// response format to server
interface IServerGetStaff {
    totalPage: number;
    currentPage: number;
    success: boolean;
    list: IStaff[];
}

interface IAdminInfo {
    id: number | string;
    type: number;
    name: string;
    email: string;
    expired: number;
    token: string;
    refreshToken: string;
}

interface IServerAdminInfo {
    success: boolean;
    info:IAdminInfo;
}

interface IServerRefreshToken {
    success: boolean;
    token: string;
    expired: number;
}

interface IServerCommonFail {
    success: boolean;
    errorCode: number;
    errorDetail: string;
}

interface IServerCommonSuccess {
    success: boolean;
}

export {
  IPermission,
  ITokenInfo,
  IFilter,
  IStaff,
  IAdminInfo,
  IServerGetStaff,
  IServerAdminInfo,
  IServerRefreshToken,
  IServerCommonFail,
  IServerCommonSuccess,
  IServerGetStaffRequest,
  IServerAddStaffRequest,
  IServerUpdateStaffRequest,
  IServerLoginRequest,
  IServerRefreshTokenRequest,
};
