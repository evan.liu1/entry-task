import {ADD_STAFF} from "../data/staff/constants";
import {IAdminInfo, IPermission, IServerAdminInfo} from "./interface";
import { userType } from './constants';
const isSuccess = (res: any) => res && res.status === 200 && res.data && res.data.success;

// detect first level props
const isShallowSame = (a, b) => {
  if (!a && !b) {
    return true;
  }
  if (a && !b) {
    return false;
  }
  if (!a && b) {
    return false;
  }
  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);
  if (aKeys.length !== bKeys.length) {
    return false;
  }

  for (let i = 0; i < aKeys.length; i++) {
    if (a[aKeys[i]] !== b[aKeys[i]]) {
      return false;
    }
  }

  return true;
};

// millisecond, will have some instrumental error， ignore cos it's only two year duration
const getDurationDisplay = (time) => {
  const monthsTimes = 3600 * 24 * 30 * 1000;
  const totalMonth = Math.floor(time / monthsTimes);
  const year = Math.floor(totalMonth / 12);
  let ret = '';
  if (year > 0) {
    ret = `${year} years `;
  }
  const month = totalMonth % 12;
  if (month > 0) {
    ret += `${month} months`;
  }
  if (!ret) {
    ret = 'less than 1 month';
  }
  return ret;
};

const startWithCharacter = (str) => {
  if (!str || str.length === 0) {
    return false;
  }
  const first = str.charAt(0).toLowerCase();
  if (first <= 'z' && first >= 'a') {
    return true;
  }
  return false;
};

// remove when set to undefined

const tokenKey = '_token';
const tokenExpiredKey = '_tokenExpired';
const refreshTokenKey = '_refreshToken';

const getToken = () => localStorage.getItem(tokenKey) || undefined;

// 0 mean un exist
const getTokenExpired = ():number => {
  const expired = localStorage.getItem(tokenExpiredKey) || undefined;
  if (expired) {
    return parseInt(expired, 10);
  }
  return 0;
};

const getRefreshToken = () => localStorage.getItem(refreshTokenKey) || undefined;

const setToken = (token: string | undefined) => {
  if (!token) {
    localStorage.removeItem(tokenKey);
    return;
  }
  localStorage.setItem(tokenKey, token);
};

const setTokenExpired = (expired: string | undefined) => {
  if (!expired) {
    localStorage.removeItem(tokenExpiredKey);
    return;
  }
  localStorage.setItem(tokenExpiredKey, expired);
};

const setRefreshToken = (refreshToken: string | undefined) => {
  if (!refreshToken) {
    localStorage.removeItem(refreshTokenKey);
    return;
  }
  localStorage.setItem(refreshTokenKey, refreshToken);
};

const getPermission = (info: IAdminInfo):IPermission =>{
  return {
    addStaff: info && info.type === userType.ADMIN,
    editStaff: info && info.type === userType.ADMIN,
    showMenu: !!(info && info.id),
  };
};

export {
  getPermission,
  isShallowSame,
  isSuccess,
  getDurationDisplay,
  startWithCharacter,
  setToken,
  getToken,
  setRefreshToken,
  getRefreshToken,
  getTokenExpired,
  setTokenExpired,
};
