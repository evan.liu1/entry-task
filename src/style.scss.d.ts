declare namespace StyleScssNamespace {
  export interface IStyleScss {
    aside: string;
    breadcrumbWrap: string;
    content: string;
    header: string;
    main: string;
    name: string;
    nameWrap: string;
    nonMenuWrap: string;
    pageTitle: string;
    popInfoEmail: string;
    popInfoIcon: string;
    popInfoName: string;
    popInfoWrap: string;
    popLogoutWrap: string;
    popWrap: string;
    wrap: string;
  }
}

declare const StyleScssModule: StyleScssNamespace.IStyleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StyleScssNamespace.IStyleScss;
};

export = StyleScssModule;
