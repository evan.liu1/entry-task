/* eslint-disable no-unused-vars */
import * as React from 'react';
import '@shopee/antd/dist/antd.css';
import {
  Switch, Route, useHistory, useLocation,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { Popover, Icon } from '@shopee/antd';
import Bars from './component/icon/bars';
import BreadcrumbByRouter from './component/breadcrumbByRouter';
// eslint-disable-next-line no-unused-vars
import Table from './container/table';
import Login from './container/login';
import MainMenu from './component/mainMenu';
import Chart from './container/chart';
import NonePermission from './container/nonePermission';
import { requestStatus, routerPath } from './utils/constants';

import style from './style.scss';
import { IAdminInfo } from './utils/interface';
import adminActions from './data/admin/actions';

const { useEffect } = React;

interface IProp {
  info: IAdminInfo,
  getInfoRequestStatus: string,
  getInfo: Function,
  logout: Function,
}

const PopInfo = (props: { info: IAdminInfo, logout: Function }) => {
  const { info, logout } = props;
  return (
    <div className={style.popWrap}>
      <div className={style.popInfoWrap}>
        <span className={style.popInfoIcon} />
        <div>
          <div className={style.popInfoName}>{info.name}</div>
          <div className={style.popInfoEmail}>{info.email}</div>
        </div>
      </div>
      <div
        onClick={() => {
          logout();
        }}
        className={style.popLogoutWrap}
      >
        <Icon type="logout" />
        <div>Logout</div>
      </div>
    </div>
  );
};

const Main = (props: IProp) => {
  const {
    info, getInfoRequestStatus, getInfo, logout,
  } = props;
  const history = useHistory();
  const location = useLocation();
  useEffect(() => {
    // fetch info when init webside
    getInfo();
  }, []);

  const path = location.pathname;
  const authPage = ['/1', 'chart'].indexOf(path) >= 0;

  const hasGetInfo = getInfoRequestStatus === requestStatus.SUCCESS
  || getInfoRequestStatus === requestStatus.FAIL;
  const hasInfo = !!(info && info.id);
  const showLogin = hasGetInfo && !hasInfo;

  return (
    <div className={style.wrap}>
      <header className={style.header}>
        <Bars />
        <h2>Information Management System</h2>
        <div className={style.nameWrap}>
          { showLogin
            && (
            <span onClick={() => {
              history.push(routerPath.LOGIN);
            }}
            >
              Log in
            </span>
            )
          }
          {
            hasInfo
            && (
            <Popover
              placement="bottomRight"
              content={<PopInfo info={info} logout={logout} />}
              title={null}
              trigger="hover"
            >
              <span className={style.name}>{info.name.substr(0, 1).toUpperCase()}</span>
            </Popover>
            )
          }
        </div>
      </header>
      <div className={style.main}>
        <div className={style.aside}>
          <MainMenu info={info} />
        </div>
        <div className={style.content}>
          {authPage
          && (
          <div className={style.breadcrumbWrap}>
            <BreadcrumbByRouter />
          </div>
          )
          }
          <Switch>
            <Route path={routerPath.CHART}>
              <Chart />
            </Route>
            <Route path={routerPath.NO_PERMISSION}>
              <NonePermission />
            </Route>
            <Route path={routerPath.LOGIN}>
              <Login />
            </Route>
            <Route path={routerPath.HOME}>
              {/* <Login /> */}
              {/* <NonePermission /> */}
              <Table />
            </Route>
          </Switch>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  info: state.admin.info,
  getInfoRequestStatus: state.admin.getInfoRequestStatus,
});

const mapDispatchToProps = (dispatch) => ({
  getInfo: () => {
    dispatch(adminActions.getInfo());
  },
  logout: () => {
    dispatch(adminActions.logout());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
