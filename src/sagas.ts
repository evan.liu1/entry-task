import { all } from 'redux-saga/effects';
import staffSaga from './data/staff/saga';
import adminSaga from './data/admin/saga';

export default function* rootSaga() {
  yield all([...staffSaga, ...adminSaga]);
}
