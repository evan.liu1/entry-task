import {
  LOGIN, LOGIN_SUCCESS, LOGIN_FAIL,
  GET_INFO, GET_INFO_SUCCESS, GET_INFO_FAIL, LOGOUT_SUCCESS
} from './constants';

import { requestStatus } from '../../utils/constants';
import { IAdminInfo, IServerAdminInfo } from '../../utils/interface';

interface IState {
  info: IAdminInfo | undefined,
  loginRequestStatus: string,
  getInfoRequestStatus: string,
}

const admin = (
  state: IState = {
    info: undefined,
    loginRequestStatus: requestStatus.IDLE,
    getInfoRequestStatus: requestStatus.IDLE,
  },
  action: any,
) => {
  switch (action.type) {
    case LOGIN:
      return Object.assign({}, state, {
        loginRequestStatus: requestStatus.REQUESTING,
      });
    case LOGIN_SUCCESS: {
      const result: IServerAdminInfo = action.data;
      return Object.assign({}, state, {
        info: result.info,
        loginRequestStatus: requestStatus.SUCCESS,
      });
    }
    case LOGIN_FAIL:
      return Object.assign({}, state, {
        loginRequestStatus: requestStatus.FAIL,
      });
    case GET_INFO:
      return Object.assign({}, state, {
        getInfoRequestStatus: requestStatus.REQUESTING,
      });
    case GET_INFO_SUCCESS: {
      const result: IServerAdminInfo = action.data;
      return Object.assign({}, state, {
        info: result.info,
        getInfoRequestStatus: requestStatus.SUCCESS,
      });
    }
    case GET_INFO_FAIL:
      return Object.assign({}, state, {
        getInfoRequestStatus: requestStatus.FAIL,
      });
    // return init state
    case LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        info: undefined,
      });
    default:
      return state;
  }
};

export default admin;
