import { put, takeLatest } from 'redux-saga/effects';
import { message } from '@shopee/antd';
import { push } from 'connected-react-router';
import {
  LOGIN,
  LOGOUT,
  GET_INFO,
} from './constants';
import adminAction from './actions';
import request from '../../utils/request';
import { routerPath } from '../../utils/constants';
import {
  IServerAdminInfo,
  IServerLoginRequest,
} from '../../utils/interface';
import {
  getToken,
  isSuccess, setRefreshToken, setToken, setTokenExpired,
} from '../../utils/helperFun';

export function* login(action) {
  const reqData:IServerLoginRequest = action.data;
  const res = yield request('/admin/login', {
    method: 'POST',
    data: reqData,
  });
  if (isSuccess(res)) {
    const repData:IServerAdminInfo = res.data;
    // set token info to localstorage
    setToken(repData.info.token);
    setRefreshToken(repData.info.refreshToken);
    setTokenExpired(`${repData.info.expired}`);

    // direct to home page after login
    yield put(push(routerPath.HOME));
    message.success('Login success!');
    yield put(adminAction.loginSuccess(res.data));
  } else {
    yield put(adminAction.loginFail());
  }
}

export function* getInfo() {
  const token = getToken();
  if (token) {
    const res = yield request('/admin/info', { customErrorHandle: true });
    if (isSuccess(res)) {
      const repData:IServerAdminInfo = res.data;
      // set token info to localstorage
      setToken(repData.info.token);
      setRefreshToken(repData.info.refreshToken);
      setTokenExpired(`${repData.info.expired}`);

      yield put(adminAction.getInfoSuccess(res.data));
      return;
    }
  }

  message.info('Please login first');
  // direct to none permission page
  yield put(push(routerPath.NO_PERMISSION));

  yield put(adminAction.getInfoFail());
}

export function* logout() {
  yield request('/admin/logout', {
    method: 'POST',
    customErrorHandle: true,
  });
  // don't detect success or not
  message.success('Logout success!');
  // clear localstorage
  localStorage.clear();
  yield put(push(routerPath.NO_PERMISSION));
  yield put(adminAction.logoutSuccess());
}

export function* loginWatch() {
  yield takeLatest(LOGIN, login);
}

export function* logoutWatch() {
  yield takeLatest(LOGOUT, logout);
}

export function* getInfoWatch() {
  yield takeLatest(GET_INFO, getInfo);
}

export default [loginWatch(), getInfoWatch(), logoutWatch()];
