import {
  LOGIN, LOGIN_SUCCESS, LOGIN_FAIL,
  GET_INFO, GET_INFO_SUCCESS, GET_INFO_FAIL,
  LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAIL,
} from './constants';

import { IServerAdminInfo, IServerLoginRequest } from '../../utils/interface';

const logout = () => ({
  type: LOGOUT,
});

const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
});

const logoutFail = () => ({
  type: LOGOUT_FAIL,
});

const login = (data:IServerLoginRequest) => ({
  type: LOGIN,
  data,
});

const loginSuccess = (data:IServerAdminInfo) => ({
  type: LOGIN_SUCCESS,
  data,
});

const loginFail = () => ({
  type: LOGIN_FAIL,
});

// auth with header token
const getInfo = () => ({
  type: GET_INFO,
});

const getInfoSuccess = (data:IServerAdminInfo) => ({
  type: GET_INFO_SUCCESS,
  data,
});

const getInfoFail = () => ({
  type: GET_INFO_FAIL,
});

export default {
  logout,
  logoutSuccess,
  logoutFail,
  login,
  loginSuccess,
  loginFail,
  getInfo,
  getInfoSuccess,
  getInfoFail,
};
