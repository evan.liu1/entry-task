import {
  ADD_STAFF, ADD_STAFF_SUCCESS, ADD_STAFF_FAIL,
  GET_STAFF_LIST, GET_STAFF_LIST_SUCCESS, GET_STAFF_LIST_FAIL,
  DELETE_STAFF, DELETE_STAFF_SUCCESS, DELETE_STAFF_FAIL,
  UPDATE_STAFF, UPDATE_STAFF_SUCCESS, UPDATE_STAFF_FAIL,
} from './constants';

import { LOGOUT_SUCCESS } from "../admin/constants";

import { requestStatus } from '../../utils/constants';
import { IServerGetStaff } from '../../utils/interface';

const staff = (
  state = {
    staff: {},
    totalPage: 0,
    currentPage: 1,
    addStaffRequestStatus: requestStatus.IDLE,
    getStaffRequestStatus: requestStatus.IDLE,
    deleteStaffRequestStatus: requestStatus.IDLE,
    updateStaffRequestStatus: requestStatus.IDLE,
  },
  action: any,
) => {
  switch (action.type) {
    case ADD_STAFF:
      return Object.assign({}, state, {
        addStaffRequestStatus: requestStatus.REQUESTING,
      });
    case ADD_STAFF_SUCCESS:
      return Object.assign({}, state, {
        addStaffRequestStatus: requestStatus.SUCCESS,
      });
    case ADD_STAFF_FAIL:
      return Object.assign({}, state, {
        addStaffRequestStatus: requestStatus.FAIL,
      });
    case GET_STAFF_LIST:
      return Object.assign({}, state, {
        getStaffRequestStatus: requestStatus.REQUESTING,
      });
    case GET_STAFF_LIST_SUCCESS: {
      const result: IServerGetStaff = action.data;
      const { currentPage } = result;
      const { list } = result;
      const { totalPage } = result;
      const key = `_${currentPage}`;
      const currentStaff = state.staff;
      currentStaff[key] = list;
      return Object.assign({}, state, {
        getStaffRequestStatus: requestStatus.SUCCESS,
        staff: Object.assign({}, currentStaff),
        totalPage,
      });
    }
    case GET_STAFF_LIST_FAIL: {
      return Object.assign({}, state, {
        getStaffRequestStatus: requestStatus.FAIL,
      });
    }
    case DELETE_STAFF:
      return Object.assign({}, state, {
        deleteStaffRequestStatus: requestStatus.REQUESTING,
      });
    case DELETE_STAFF_SUCCESS:
      return Object.assign({}, state, {
        deleteStaffRequestStatus: requestStatus.SUCCESS,
      });
    case DELETE_STAFF_FAIL:
      return Object.assign({}, state, {
        deleteStaffRequestStatus: requestStatus.FAIL,
      });
    case UPDATE_STAFF:
      return Object.assign({}, state, {
        updateStaffRequestStatus: requestStatus.REQUESTING,
      });
    case UPDATE_STAFF_SUCCESS:
      return Object.assign({}, state, {
        updateStaffRequestStatus: requestStatus.SUCCESS,
      });
    case UPDATE_STAFF_FAIL:
      return Object.assign({}, state, {
        updateStaffRequestStatus: requestStatus.FAIL,
      });
    // return init state
    case LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        staff: {},
        totalPage: 0,
        currentPage: 1,
        addStaffRequestStatus: requestStatus.IDLE,
        getStaffRequestStatus: requestStatus.IDLE,
        deleteStaffRequestStatus: requestStatus.IDLE,
        updateStaffRequestStatus: requestStatus.IDLE,
      });
    default:
      return state;
  }
};

export default staff;
