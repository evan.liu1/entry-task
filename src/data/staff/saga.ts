import { put, takeEvery, takeLatest } from 'redux-saga/effects';
import { message } from '@shopee/antd';
import {
  ADD_STAFF, GET_STAFF_LIST, DELETE_STAFF, UPDATE_STAFF,
} from './constants';
import staffAction from './actions';
import request from '../../utils/request';
import {
  IServerAddStaffRequest, IServerUpdateStaffRequest, IServerGetStaffRequest,
} from '../../utils/interface';
import { isSuccess } from '../../utils/helperFun';

export function* addStaff(action) {
  const addData:IServerAddStaffRequest = action.data;
  const res = yield request('/staff/add', {
    method: 'PUT',
    data: addData,
  });
  if (isSuccess(res)) {
    message.success('Add success!');
    yield put(staffAction.addStaffSuccess());
  } else {
    yield put(staffAction.addStaffFail());
  }
}

export function* getStaffList(action) {
  const data:IServerGetStaffRequest = {
    currentPage: action.currentPage || 1,
    size: action.size || 4,
  };
  if (action.filter) {
    data.filter = action.filter;
  }
  const res = yield request('/staff/get', {
    params: data,
  });
  if (isSuccess(res)) {
    yield put(staffAction.getStaffListSuccess(res.data, !!action.clearCache));
  }
}

export function* updateStaff(action) {
  const url = `/staff/update/${action.id}`;
  const data:IServerUpdateStaffRequest = {
    name: action.name,
  };
  const res = yield request(url, {
    method: 'POST',
    data,
  });
  if (isSuccess(res)) {
    message.success('Update staff name success!');
    yield put(staffAction.updateStaffSuccess());
  } else {
    yield put(staffAction.updateStaffFail());
  }
}

export function* deleteStaff(action) {
  const url = `/staff/delete/${action.id}`;
  const res = yield request(url, {
    method: 'DELETE',
  });
  if (isSuccess(res)) {
    message.success('Delete success!');
    yield put(staffAction.deleteStaffSuccess());
  } else {
    yield put(staffAction.deleteStaffFail());
  }
}

export function* getStaffListWatch() {
  yield takeEvery(GET_STAFF_LIST, getStaffList);
}

export function* addStaffWatch() {
  yield takeEvery(ADD_STAFF, addStaff);
}

export function* deleteStaffWatch() {
  yield takeLatest(DELETE_STAFF, deleteStaff);
}

export function* updateStaffWatch() {
  yield takeLatest(UPDATE_STAFF, updateStaff);
}

export default [getStaffListWatch(), addStaffWatch(), deleteStaffWatch(), updateStaffWatch()];
