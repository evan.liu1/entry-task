import {
  ADD_STAFF, ADD_STAFF_FAIL, ADD_STAFF_SUCCESS,
  GET_STAFF_LIST, GET_STAFF_LIST_FAIL, GET_STAFF_LIST_SUCCESS,
  DELETE_STAFF, DELETE_STAFF_SUCCESS, DELETE_STAFF_FAIL,
  UPDATE_STAFF, UPDATE_STAFF_SUCCESS, UPDATE_STAFF_FAIL,
} from './constants';

import { IFilter, IServerAddStaffRequest, IServerGetStaff } from '../../utils/interface';

const addStaff = (data:IServerAddStaffRequest|IServerAddStaffRequest[]) => ({
  type: ADD_STAFF,
  data,
});

const addStaffSuccess = () => ({
  type: ADD_STAFF_SUCCESS,
});

const addStaffFail = () => ({
  type: ADD_STAFF_FAIL,
});

const getStaffList = (currentPage: number, size: number, filter: null | IFilter,
  clearCache?: boolean) => ({
  type: GET_STAFF_LIST,
  filter,
  currentPage,
  size,
  clearCache,
});

const getStaffListSuccess = (data: IServerGetStaff, clearCache?: boolean) => ({
  type: GET_STAFF_LIST_SUCCESS,
  data,
  clearCache,
});

const getStaffListFail = () => ({
  type: GET_STAFF_LIST_FAIL,
});

const deleteStaff = (id: number|string) => ({
  type: DELETE_STAFF,
  id,
});

const deleteStaffSuccess = () => ({
  type: DELETE_STAFF_SUCCESS,
});

const deleteStaffFail = () => ({
  type: DELETE_STAFF_FAIL,
});

const updateStaff = (id: number|string, name: string) => ({
  type: UPDATE_STAFF,
  name,
  id,
});

const updateStaffSuccess = () => ({
  type: UPDATE_STAFF_SUCCESS,
});

const updateStaffFail = () => ({
  type: UPDATE_STAFF_FAIL,
});

export default {
  addStaff,
  addStaffSuccess,
  addStaffFail,
  getStaffList,
  getStaffListSuccess,
  getStaffListFail,
  deleteStaff,
  deleteStaffSuccess,
  deleteStaffFail,
  updateStaff,
  updateStaffSuccess,
  updateStaffFail,
};
