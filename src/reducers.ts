import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import staff from './data/staff/reducer';
import admin from './data/admin/reducer';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  staff,
  admin,
});
export default createRootReducer;
