import * as React from 'react';
import * as ReactDOM from 'react-dom';
import '@shopee/antd/dist/antd.css';
import {
  BrowserRouter as Router, Switch, Route, Link,
} from 'react-router-dom';
import { ConnectedRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';
import { StoreContext } from 'redux-react-hook';
import rootSaga from './sagas';
import createRootReducer from './reducers';
import Main from './main';
const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware, thunk, routerMiddleware(history)];

const storeEnhancer = composeWithDevTools(applyMiddleware(...middlewares));
const store = createStore(createRootReducer(history), storeEnhancer);
sagaMiddleware.run(rootSaga);

const App = () => (
  <Provider store={store}>
    <StoreContext.Provider value={store}>
      <ConnectedRouter history={history}>
        <Main />
      </ConnectedRouter>
    </StoreContext.Provider>
  </Provider>
);

const wrapper = document.getElementById('root');
ReactDOM.render(<App />, wrapper);
