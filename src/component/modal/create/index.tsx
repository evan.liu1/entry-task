/* eslint-disable no-unused-vars,jsx-a11y/click-events-have-key-events */
import * as React from 'react';
import {
  DatePicker, Icon, Input, Menu, message, Modal, Select,
} from '@shopee/antd';
import { connect } from 'react-redux';
import style from './style.scss';
import staffActions from '../../../data/staff/actions';
import { jobMap, requestStatus } from '../../../utils/constants';
import { IServerAddStaffRequest } from '../../../utils/interface';
import { startWithCharacter } from '../../../utils/helperFun';
import { usePrevious } from '../../../utils/hook';

const { useState, useEffect } = React;

const { Option } = Select;
const { RangePicker } = DatePicker;

const bodyStyle = {
  padding: 0,
};

const iconStyle = {
  fontSize: 60,
};

const jobKeys = Array.from(jobMap.keys());
const pageType = {
  SINGLE_UPLOAD: '_0',
  MASS_UPLOAD: '_1',
};
let fileUploadingTimeout;

const loadFileContent = (e, callback:Function, setFileUploading: Function) => {
  // @ts-ignore
  const files = e.target.files || e.dataTransfer.files;
  if (!files || files.length < 1) {
    return;
  }
  const reader = new FileReader();
  reader.readAsText(files[0], 'utf-8');
  const { name } = files[0];
  setFileUploading(false, name);
  if (fileUploadingTimeout) {
    clearTimeout(fileUploadingTimeout);
  }
  setTimeout(() => {
    setFileUploading(true, name);
  }, 0);
  fileUploadingTimeout = setTimeout(() => {
    setFileUploading(false, name);
  }, 1000);

  // not working, coz is local file?

  // reader.addEventListener('progress', (data) => {
  //   console.log(data.loaded);
  //   if (data.lengthComputable) {
  //     const progress = parseInt(((data.loaded / data.total) * 100), 10);
  //     console.log(progress);
  //   }
  //   console.log(data);
  // });
  reader.onload = (evt) => {
    if (evt && evt.target && evt.target.result && typeof evt.target.result === 'string') {
      const text = evt.target.result;
      if (text) {
        // get staff info for upload file
        const arrayData = text.split('\n');
        if (arrayData.length < 2) {
          message.error('Data is empty');
          return;
        }
        const staffData = arrayData.slice(1, arrayData.length - 1);
        const nameMap = {};
        const addData:IServerAddStaffRequest[] = [];

        // check staff info logic
        for (let i = 0; i < staffData.length; i += 1) {
          const staffArray = staffData[i].split(',');
          if (staffArray.length !== 4) {
            message.error('Every user record must have name, job, startDate and endDate');
            return;
          }

          const add:IServerAddStaffRequest = {
            name: staffArray[0],
            job: staffArray[1],
            startDate: parseInt(staffArray[2], 10),
            endDate: parseInt(staffArray[3], 10),
          };
          if (!add.name) {
            message.error('Name is empty!');
            return;
          }
          if (!startWithCharacter(add.name)) {
            message.error('Name must start with character!');
            return;
          }
          if (nameMap[add.name]) {
            message.error(`Name[${add.name}] is multiple!`);
            return;
          }
          nameMap[add.name] = true;

          if (Number.isNaN(parseInt(add.job, 10))) {
            message.error(`${add.name}' job error`);
            return;
          }
          if (jobKeys.indexOf(parseInt(add.job, 10)) < 0) {
            message.error(`${add.name}' job not is list`);
            return;
          }
          if (Number.isNaN(add.startDate)) {
            message.error(`${add.name}' startDate format error`);
            return;
          }
          if (Number.isNaN(add.endDate)) {
            message.error(`${add.name}' etartDate format error`);
            return;
          }

          addData.push(add);
        }

        // call back to set pending mass data
        if (callback) {
          callback(addData);
        }
      }
    } else {
      message.error('Please upload csv file with require format');
    }
    e.target.value = null;
  };
};

const bindDragFunction = (callback: Function, setFileUploading: Function) => {
  const dragContent = document.getElementById('dragContent');
  const file = document.getElementById('file');

  if (!dragContent || !file) {
    return;
  }

  dragContent.addEventListener('dragover', (e) => {
    e.preventDefault();
    e.stopPropagation();
  });
  dragContent.addEventListener('dragenter', (e) => {
    e.preventDefault();
    e.stopPropagation();
  });
  dragContent.addEventListener('drop', (e) => {
    e.preventDefault();
    e.stopPropagation();
    loadFileContent(e, callback, setFileUploading);
  });

  file.addEventListener('change', (e) => {
    loadFileContent(e, callback, setFileUploading);
  });
};

interface IProps {
    show: boolean;
    addStaffRequestStatus: string;
    onCancel: Function;
    onOk: Function;
    addStaff: Function;
}

const createModal = (props:IProps) => {
  const {
    show, onCancel, onOk, addStaffRequestStatus, addStaff,
  } = props;
  // 0: single create page, 1: mass create page
  const [currentPage, setCurrentPage] = useState(pageType.SINGLE_UPLOAD);
  // eslint-disable-next-line max-len
  const [pendingMassData, setPendingMassData] = useState<undefined|IServerAddStaffRequest[]>(undefined);
  const [name, setName] = useState('');
  const [isFileUploading, setIsFileUploading] = useState(false);
  const [fileName, setFileName] = useState<undefined|string>(undefined);
  const [jobValue, setJobValue] = useState(undefined);
  const [startTime, setStartTime] = useState<any>(undefined);
  const [endTime, setEndTime] = useState<any>(undefined);
  const preRequestStatus:any = usePrevious({
    addStaffRequestStatus,
  });
    // close after add success
  useEffect(() => {
    if (preRequestStatus && preRequestStatus.addStaffRequestStatus === requestStatus.REQUESTING
            && addStaffRequestStatus === requestStatus.SUCCESS
    ) {
      if (typeof onOk === 'function') {
        onOk();
      }
    }
  }, [addStaffRequestStatus]);

  const onDragClick = () => {
    const file = document.getElementById('file');
    if (file) {
      file.click();
    }
  };

  let fileWrapClass = isFileUploading ? `${style.fileWrap} ${style.fileWrapUploading}` : style.fileWrap;
  if (!fileName) {
    fileWrapClass = style.dragContentWrapNone;
  }

  return (
    <Modal
      title="Create Person"
      className={style.wrap}
      centered
      destroyOnClose
      afterClose={() => {
        if (fileUploadingTimeout) {
          clearTimeout(fileUploadingTimeout);
        }
        setFileName(undefined);
        setIsFileUploading(false);
      }}
      bodyStyle={bodyStyle}
      visible={show}
      okText="Submit"
      confirmLoading={addStaffRequestStatus === requestStatus.REQUESTING || isFileUploading}
      onOk={() => {
        if (currentPage === pageType.MASS_UPLOAD) {
          // use pending mass data which already check when upload file
          if (!pendingMassData || pendingMassData.length === 0) {
            message.error('You haven\'t add any file');
          }
          addStaff(pendingMassData);
          return;
        }

        // add single data before check
        if (!name) {
          message.error('Name is empty!');
          return;
        }
        if (!startWithCharacter(name)) {
          message.error('Name must start with character!');
          return;
        }
        if (!jobValue) {
          message.error('Job is empty!');
          return;
        }
        if (!startTime || !endTime) {
          message.error('Entry time is empty!');
          return;
        }
        // entry time no longer than 2 year;
        const millisecondsStart = startTime.valueOf();
        const millisecondsEnd = endTime.valueOf();
        const maxTimeGap = 3600 * 24 * 365 * 2 * 1000;
        if (millisecondsEnd - millisecondsStart > maxTimeGap) {
          message.error('Entry Date shouldn\'t longer than 2 years!');
        }
        const data:IServerAddStaffRequest = {
          name,
          job: `${jobValue}`,
          startDate: startTime.valueOf(),
          endDate: endTime.valueOf(),
        };
        addStaff(data);
      }}
      onCancel={() => {
        onCancel();
      }}
    >
      <div className={style.content}>
        <div className={style.menu}>
          <Menu
            onClick={({ item, key }) => {
              setCurrentPage(key);
              if (key === pageType.MASS_UPLOAD) {
                bindDragFunction((data:IServerAddStaffRequest[]) => {
                  setPendingMassData(data);
                }, (isUploading:boolean, uploadFileName: undefined | string) => {
                  setFileName(uploadFileName);
                  setIsFileUploading(isUploading);
                });
              }
            }}
            style={{ width: 120 }}
            defaultSelectedKeys={[currentPage]}
          >
            <Menu.Item key={pageType.SINGLE_UPLOAD}>
              Single Create
            </Menu.Item>
            <Menu.Item key={pageType.MASS_UPLOAD}>
              Mass Create
            </Menu.Item>
          </Menu>
        </div>
        <div className={style.main}>
          {/* single add function */}
          {
                currentPage === pageType.SINGLE_UPLOAD && (
                <div className={style.inputWrap}>
                  <div className={style.inputItem}>
                    <span className={style.inputItemLabel}>Name</span>
                    <Input
                      value={name}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                      className={style.inputItemContent}
                      placeholder="String Only"
                    />
                  </div>
                  <div className={style.inputItem}>
                    <span className={style.inputItemLabel}>Job Description</span>
                    <Select
                      showSearch
                      placeholder="Select"
                      className={style.inputItemContent}
                      value={jobValue}
                      onChange={(value) => {
                        setJobValue(value);
                      }}
                    >
                      {
                                jobKeys.map((key) => (
                                  <Option value={key} key={key}>{jobMap.get(key)}</Option>
                                ))
                            }
                    </Select>
                  </div>
                  <div className={style.inputItem}>
                    <span className={style.inputItemLabel}>Entry Date</span>
                    <RangePicker
                      placeholder={['Start Date', 'End Date']}
                      className={style.inputItemContent}
                      separator={'->'}
                      allowClear={false}
                      value={[startTime, endTime]}
                      onChange={(dates) => {
                        if (dates && dates.length === 2) {
                          const start = dates[0];
                          const end = dates[1];
                          if (start && end) {
                            setStartTime(start);
                            setEndTime(end);
                          }
                        }
                      }}
                    />
                  </div>

                </div>
                )
            }
          {/* mass add function */}
          <div className={currentPage === pageType.MASS_UPLOAD ? style.dragContentWrap : `${style.dragContentWrap} ${style.dragContentWrapNone}`}>
            <div
              id="dragContent"
              onClick={onDragClick}
              className={style.dragContent}
            >
              <input type="file" id="file" className={style.dragContentWrapNone} multiple />
              <Icon type="inbox" style={iconStyle} />
              <p>Click or drap files here to upload</p>
            </div>
            <div className={fileWrapClass}>
              <div className={style.fileInfo}>
                <div className={style.fileIconWrap}>
                  {isFileUploading
                  && <Icon type="loading" />
                  }
                  {!isFileUploading
                  && <Icon type="link" />
                  }
                </div>
                <div>
                  {fileName}
                </div>
              </div>
              <div className={style.fileProgress}>
                <div className={isFileUploading ? `${style.fileProgressInner} ${style.fileProgressInnerLoading}` : style.fileProgressInner} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

const mapStateToProps = (state: any) => ({
  addStaffRequestStatus: state.staff.addStaffRequestStatus,
});

const mapDispatchToProps = (dispatch: any) => ({
  addStaff: (data: IServerAddStaffRequest|IServerAddStaffRequest[]) => {
    dispatch(staffActions.addStaff(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(createModal);
