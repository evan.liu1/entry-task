declare namespace StyleScssNamespace {
  export interface IStyleScss {
    content: string;
    dragContent: string;
    dragContentWrap: string;
    dragContentWrapNone: string;
    fileIconWrap: string;
    fileInfo: string;
    fileProgress: string;
    fileProgressInner: string;
    fileProgressInnerLoading: string;
    fileWrap: string;
    fileWrapUploading: string;
    inputItem: string;
    inputItemContent: string;
    inputItemLabel: string;
    inputWrap: string;
    main: string;
    menu: string;
    menuItem: string;
    pageTitle: string;
    wrap: string;
  }
}

declare const StyleScssModule: StyleScssNamespace.IStyleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StyleScssNamespace.IStyleScss;
};

export = StyleScssModule;
