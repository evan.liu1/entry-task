import * as React from 'react';
import { Table, Popconfirm } from '@shopee/antd';
import { connect } from 'react-redux';
import style from './style.scss';
import { jobMap } from '../../utils/constants';
import staffActions from '../../data/staff/actions';
import { getDurationDisplay, startWithCharacter } from '../../utils/helperFun';
import { IStaff } from '../../utils/interface';

const { useState } = React;

interface IProps {
  data: IStaff[],
  isLoading: boolean,
  editable: boolean,
  deleteStaff: Function,
  updateStaff: Function,
}

const StaffTable = (
  props: IProps,
) => {
  const {
    data, isLoading, editable, deleteStaff, updateStaff,
  } = props;
  const [errorMap, setErrorMap] = useState({});
  // minus header height + input area + bread scamp + padding
  let fixHeight = document.body.clientHeight - 58 - 186 - 68 - 60 - 32 - 40;
  if (!editable) {
    fixHeight += 60;
  }
  const renderName = (text, record) => {
    const error = errorMap[record.id];
    if (editable) {
      return (
      // return for admin user
        <div className={error ? `${style.editInputWrap} ${style.editInputWrapError}` : style.editInputWrap}>
          <input
            onBlur={(e) => {
              const newName = e.target.value;
              if (!newName || !startWithCharacter(newName) || newName === text) {
                e.target.value = text;
                setErrorMap({});
              } else {
                updateStaff(record.id, newName);
                setErrorMap({});
              }
            }}
            maxLength={30}
            className={style.editInput}
            placeholder="Enter"
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                const targetInput = e.target;
                // @ts-ignore
                const newName = targetInput.value;
                if (newName && startWithCharacter(newName) && newName !== text) {
                  // @ts-ignore
                  targetInput.blur();
                }
              }
            }}
            onChange={(e) => {
              const nameValue = e.target.value;
              const newErrorMap = {};
              if (!nameValue) {
                newErrorMap[record.id] = 'This field is required';
                setErrorMap(Object.assign({}, newErrorMap));
                return;
              }
              if (!startWithCharacter(nameValue)) {
                newErrorMap[record.id] = 'Name should star with character';
                setErrorMap(Object.assign({}, newErrorMap));
                return;
              }
              setErrorMap({});
            }}
            defaultValue={text}
          />
          { error && <p className={style.error}>{errorMap[record.id]}</p>}
        </div>
      );
    }
    // normal user just return span
    return (<span>{text}</span>);
  };

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '30%',
      render: renderName,
    },
    {
      title: 'Job Description',
      dataIndex: 'job',
      key: 'job',
      width: '30%',
      render: (text) => {
        const display = jobMap.get(parseInt(text, 10));
        return (<span>{display}</span>);
      },
    },
    {
      title: 'Duration',
      dataIndex: 'duration',
      key: 'duration',
      width: '20%',
      render: (text) => {
        const durationDisplay = getDurationDisplay(text);
        return (<span>{durationDisplay}</span>);
      },
    },
    {
      title: 'Action',
      key: 'action',
      width: '20%',
      render: (text, record) => {
        const title = <span className={style.deleteTitle}>Are you sure to delete？</span>;
        return (
          <Popconfirm
            onConfirm={() => {
              deleteStaff(record.id);
            }}
            title={title}
            okText="Delete"
            cancelText="Cancel"
          >
            <span className={style.delete}>
        Delete
            </span>
          </Popconfirm>
        );
      },
    },
  ];

  return (
    <div
      className={style.wrap}
    >
      <Table rowKey="id" scroll={{ y: fixHeight }} loading={isLoading} columns={columns} dataSource={data} pagination={false} />
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => ({
  deleteStaff: (id: string | number) => {
    dispatch(staffActions.deleteStaff(id));
  },
  updateStaff: (id: string | number, name: string) => {
    dispatch(staffActions.updateStaff(id, name));
  },
});

// @ts-ignore
export default connect(null, mapDispatchToProps)(StaffTable);
