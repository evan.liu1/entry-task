declare namespace StyleScssNamespace {
  export interface IStyleScss {
    delete: string;
    deleteTitle: string;
    editInput: string;
    editInputWrap: string;
    editInputWrapError: string;
    error: string;
    pageTitle: string;
    wrap: string;
  }
}

declare const StyleScssModule: StyleScssNamespace.IStyleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: StyleScssNamespace.IStyleScss;
};

export = StyleScssModule;
