import * as React from 'react';
import {
  Menu, Icon,
} from '@shopee/antd';
import '@shopee/antd/dist/antd.css';
import {
  useHistory,
} from 'react-router-dom';
import style from './style.scss';
import { routerPath } from '../../utils/constants';
import { IAdminInfo } from '../../utils/interface';
import { getPermission } from '../../utils/helperFun';

const { SubMenu } = Menu;

interface IProps {
    info: IAdminInfo,
}

const MenuComponent = (props: IProps) => {
  const { info } = props;
  const { showMenu } = getPermission(info);
  const history = useHistory();

  if (!showMenu) {
    return (
      <div className={style.nonMenuWrap}>
        <Icon type="pushpin" />
        <span>System Management</span>
      </div>
    );
  }
  const openTablePage = () => {
    history.push(routerPath.HOME);
  };

  const openChartPage = () => {
    history.push(routerPath.CHART);
  };
  return (
    <Menu
      style={{ width: 240 }}
      mode="inline"
      defaultSelectedKeys={['1']}
      defaultOpenKeys={['sub1']}
    >
      <SubMenu
        key="sub1"
        title={(
          <span>
            <Icon type="pushpin" />
            <span>System Management</span>
          </span>
                )}
      >
        <Menu.Item key="1" onClick={openTablePage}>Table</Menu.Item>
        <Menu.Item key="2" onClick={openChartPage}>Chart</Menu.Item>
      </SubMenu>
    </Menu>
  );
};

export default MenuComponent;
