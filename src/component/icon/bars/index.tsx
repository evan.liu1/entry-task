import * as React from 'react';
import style from './style.scss';

const bars = ({ width = 18, height = 15 }) => (
  <div
    className={style.wrap}
    style={{
      width: `${width}px`,
      height: `${height}px`,
    }}
  >
    <div className={style.item} />
    <div className={style.item} />
    <div className={style.item} />
  </div>
);

export default bars;
