import * as React from 'react';
import { Breadcrumb } from '@shopee/antd';
import { useLocation } from 'react-router-dom';

const breadcrumbByRouter = () => {
  const location = useLocation();
  const currentPage = location.pathname === '/' ? 'Table Page' : 'Chart Page';
  return (
    <Breadcrumb>
      <Breadcrumb.Item>System Management</Breadcrumb.Item>
      <Breadcrumb.Item>
        { currentPage }
      </Breadcrumb.Item>
    </Breadcrumb>
  );
};

export default breadcrumbByRouter;
